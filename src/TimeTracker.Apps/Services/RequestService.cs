﻿using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TimeTracker.Apps.Services;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Accounts;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Authentications.Credentials;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.Requests
{
    public class RequestService
    {

        private async Task<Response<T>> CheckResponseAsync<T>(HttpResponseMessage httpResponseMessage)
        {
            string content = httpResponseMessage.Content.ReadAsStringAsync().Result;
            Response<T> response = JsonConvert.DeserializeObject<Response<T>>(content);
            if (!response.IsSuccess)
            {
                Debug.WriteLine(httpResponseMessage);
                Debug.WriteLine("Error code : {0}, message : {1}", response.ErrorCode, response.ErrorMessage);
                await new DialogService().DisplayAlertAsync(response.ErrorCode, response.ErrorMessage, "OK");
            }
            return response;
        }

        public sealed class Void
        {
            // Cette classe permet d'utiliser les fonction contenant un type T, mais avec T qui n'a aucune donnée
            // (Cela évite de réécrire certaine fonction en double pour Response et Response<T>)
            private Void() { }
        }

        public async Task<bool> CheckTokenValidAsync(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
            {
                await DependencyService.Get<LoginService>().RefreshTokenAsync();
                return false;
            }
            return true;
        }


        private async Task<Response<T>> HttpGetAsync<T>(string url)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage httpResponse = await client.GetAsync(url);
            return await CheckResponseAsync<T>(httpResponse);
        }

        private async Task<Response<T>> HttpGetWithTokenAsync<T>(string url)
        {
            HttpClient client = new HttpClient();

            LoginService loginService = DependencyService.Get<LoginService>();
            string token = await loginService.GetAccessToken();
            if (token == null)
                return null;
            client.DefaultRequestHeaders.Add("Authorization", token);

            HttpResponseMessage httpResponse = await client.GetAsync(url);
            if (!await CheckTokenValidAsync(httpResponse))
            {
                // On recommence la requête avec le token mis à jour
                client.DefaultRequestHeaders.Clear();
                token = await loginService.GetAccessToken();
                if (token == null)
                    return null;
                client.DefaultRequestHeaders.Add("Authorization", token);
                httpResponse = await client.GetAsync(url);
            }

            return await CheckResponseAsync<T>(httpResponse);
        }

        private async Task<Response<T>> HttpPostAsync<T>(string url, HttpContent httpContent)
        {
            HttpClient client = new HttpClient();
            Task<HttpResponseMessage> httpResponse = client.PostAsync(url, httpContent);
            return await CheckResponseAsync<T>(httpResponse.Result);
        }


        private async Task<Response<T>> HttpPostWithTokenAsync<T>(string url, HttpContent httpContent)
        {
            HttpClient client = new HttpClient();

            LoginService loginService = DependencyService.Get<LoginService>();
            string token = await loginService.GetAccessToken();
            if (token == null)
                return null;
            client.DefaultRequestHeaders.Add("Authorization", token);

            HttpResponseMessage httpResponse = await client.PostAsync(url, httpContent);
            if (!await CheckTokenValidAsync(httpResponse)) // Si token invalide
            {
                // On recommence la requête avec le token mis à jour
                client.DefaultRequestHeaders.Clear();
                token = await loginService.GetAccessToken();
                if (token == null)
                    return null;
                client.DefaultRequestHeaders.Add("Authorization", token);
                httpResponse = await client.GetAsync(url);
            }

            return await CheckResponseAsync<T>(httpResponse);
        }


        private async Task<Response<T>> HttpPatchWithTokenAsync<T>(string url, HttpContent httpContent)
        {
            HttpClient client = new HttpClient();

            LoginService loginService = DependencyService.Get<LoginService>();
            string token = await loginService.GetAccessToken();
            if (token == null)
                return null;
            client.DefaultRequestHeaders.Add("Authorization", token);

            HttpMethod method = new HttpMethod("PATCH");
            HttpRequestMessage request = new HttpRequestMessage(method, url) { Content = httpContent };

            HttpResponseMessage httpResponse = await client.SendAsync(request);
            if (!await CheckTokenValidAsync(httpResponse)) // Si token invalide
            {
                // On recommence la requête avec le token mis à jour
                client.DefaultRequestHeaders.Clear();
                token = await loginService.GetAccessToken();
                if (token == null)
                    return null;
                client.DefaultRequestHeaders.Add("Authorization", token);
                httpResponse = await client.SendAsync(request);
            }

            return await CheckResponseAsync<T>(httpResponse);
        }


        private async Task<Response<T>> HttpPutWithTokenAsync<T>(string url, HttpContent httpContent)
        {
            HttpClient client = new HttpClient();

            LoginService loginService = DependencyService.Get<LoginService>();
            string token = await loginService.GetAccessToken();
            if (token == null)
                return null;
            client.DefaultRequestHeaders.Add("Authorization", token);

            HttpMethod method = new HttpMethod("PUT");
            HttpRequestMessage request = new HttpRequestMessage(method, url) { Content = httpContent };

            HttpResponseMessage httpResponse = await client.SendAsync(request);
            if (!await CheckTokenValidAsync(httpResponse)) // Si token invalide
            {
                // On recommence la requête avec le token mis à jour
                client.DefaultRequestHeaders.Clear();
                token = await loginService.GetAccessToken();
                if (token == null)
                    return null;
                client.DefaultRequestHeaders.Add("Authorization", token);
                httpResponse = await client.SendAsync(request);
            }

            return await CheckResponseAsync<T>(httpResponse);
        }


        private async Task<Response<T>> HttpDeleteWithTokenAsync<T>(string url)
        {
            HttpClient client = new HttpClient();

            LoginService loginService = DependencyService.Get<LoginService>();
            string token = await loginService.GetAccessToken();
            if (token == null)
                return null;
            client.DefaultRequestHeaders.Add("Authorization", token);

            HttpResponseMessage httpResponse = await client.DeleteAsync(url);
            if (!await CheckTokenValidAsync(httpResponse)) // Si token invalide
            {
                // On recommence la requête avec le token mis à jour
                client.DefaultRequestHeaders.Clear();
                token = await loginService.GetAccessToken();
                if (token == null)
                    return null;
                client.DefaultRequestHeaders.Add("Authorization", token);
                httpResponse = await client.DeleteAsync(url);
            }

            return await CheckResponseAsync<T>(httpResponse);
        }


        public async Task<Response<LoginResponse>> RegisterRequestAsync(CreateUserRequest createUserRequest)
        {
            string url = Urls.HOST + "/" + Urls.CREATE_USER;
            string json = JsonConvert.SerializeObject(createUserRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPostAsync<LoginResponse>(url, httpContent);
        }


        public async Task<Response<LoginResponse>> LoginRequestAsync(LoginWithCredentialsRequest loginWithCredentialsRequest)
        {
            string url = Urls.HOST + "/" + Urls.LOGIN;
            string json = JsonConvert.SerializeObject(loginWithCredentialsRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPostAsync<LoginResponse>(url, httpContent);
        }


        public async Task<Response<LoginResponse>> RefreshTokenAsync(RefreshRequest refreshRequest)
        {
            string url = Urls.HOST + "/" + Urls.REFRESH_TOKEN;
            string json = JsonConvert.SerializeObject(refreshRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPostAsync<LoginResponse>(url, httpContent);
        }


        public async Task<Response> SetPasswordRequestAsync(SetPasswordRequest setPasswordRequest)
        {
            string url = Urls.HOST + "/" + Urls.SET_PASSWORD;
            string json = JsonConvert.SerializeObject(setPasswordRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPatchWithTokenAsync<Void>(url, httpContent);
        }


        public async Task<Response<UserProfileResponse>> UserProfileRequestAsync()
        {
            string url = Urls.HOST + "/" + Urls.USER_PROFILE;
            return await HttpGetWithTokenAsync<UserProfileResponse>(url);
        }


        public async Task<Response<UserProfileResponse>> SetUserProfileRequestAsync(SetUserProfileRequest setUserProfileRequest)
        {

            string url = Urls.HOST + "/" + Urls.SET_USER_PROFILE;
            string json = JsonConvert.SerializeObject(setUserProfileRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPatchWithTokenAsync<UserProfileResponse>(url, httpContent);
        }


        public async Task<Response<ObservableCollection<ProjectItem>>> ListProjectsRequestAsync()
        {
            string url = Urls.HOST + "/" + Urls.LIST_PROJECTS;
            return await HttpGetWithTokenAsync<ObservableCollection<ProjectItem>>(url);
        }


        public async Task<Response<ProjectItem>> AddProjectAsync(AddProjectRequest addProjectRequest)
        {
            string url = Urls.HOST + "/" + Urls.ADD_PROJECT;
            string json = JsonConvert.SerializeObject(addProjectRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPostWithTokenAsync<ProjectItem>(url, httpContent);
        }


        public async Task<Response<ObservableCollection<TaskItem>>> ListTasksRequestAsync(long projectId)
        {
            string url = Urls.HOST + "/" + Urls.LIST_TASKS;
            url = url.Replace("{projectId}", projectId.ToString());
            return await HttpGetWithTokenAsync<ObservableCollection<TaskItem>>(url);
        }


        public async Task<Response<TaskItem>> AddTaskRequestAsync(AddTaskRequest addTaskRequest, long projectId)
        {
            string url = Urls.HOST + "/" + Urls.CREATE_TASK;
            url = url.Replace("{projectId}", projectId.ToString());
            string json = JsonConvert.SerializeObject(addTaskRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPostWithTokenAsync<TaskItem>(url, httpContent);
        }

        public async Task<Response<TimeItem>> AddTimeTaskAsync(AddTimeRequest addTimeRequest, long projectId, long taskId)
        {
            string url = Urls.HOST + "/" + Urls.ADD_TIME;
            url = url.Replace("{projectId}", projectId.ToString());
            url = url.Replace("{taskId}", taskId.ToString());
            string json = JsonConvert.SerializeObject(addTimeRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPostWithTokenAsync<TimeItem>(url, httpContent);
        }

        public async Task<Response<TaskItem>> PutTaskRequestAsync(AddTaskRequest addTaskRequest, long projectId, long taskId)
        {
            string url = Urls.HOST + "/" + Urls.UPDATE_TASK;
            url = url.Replace("{projectId}", projectId.ToString());
            url = url.Replace("{taskId}", taskId.ToString());
            string json = JsonConvert.SerializeObject(addTaskRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPutWithTokenAsync<TaskItem>(url, httpContent);
        }


        public async Task<Response<ProjectItem>> PutProjectAsync(AddProjectRequest addProjectRequest, long projectId)
        {
            string url = Urls.HOST + "/" + Urls.UPDATE_PROJECT;
            url = url.Replace("{projectId}", projectId.ToString());
            string json = JsonConvert.SerializeObject(addProjectRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPutWithTokenAsync<ProjectItem>(url, httpContent);
        }

        public async Task<Response<TimeItem>> PutTimeTaskAsync(AddTimeRequest addTimeRequest, long projectId, long taskId, long timeId)
        {
            string url = Urls.HOST + "/" + Urls.UPDATE_TIME;
            url = url.Replace("{projectId}", projectId.ToString());
            url = url.Replace("{taskId}", taskId.ToString());
            url = url.Replace("{timeId}", timeId.ToString());
            string json = JsonConvert.SerializeObject(addTimeRequest);
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await HttpPutWithTokenAsync<TimeItem>(url, httpContent);
        }

        public async Task<Response> DeleteProjectAsync(long projectId)
        {
            string url = Urls.HOST + "/" + Urls.DELETE_PROJECT;
            url = url.Replace("{projectId}", projectId.ToString());
            return await HttpDeleteWithTokenAsync<Void>(url);
        }

        public async Task<Response> DeleteTaskAsync(long projectId, long taskId)
        {
            string url = Urls.HOST + "/" + Urls.DELETE_TASK;
            url = url.Replace("{projectId}", projectId.ToString());
            url = url.Replace("{taskId}", taskId.ToString());
            return await HttpDeleteWithTokenAsync<Void>(url);
        }

        public async Task<Response> DeleteTimeTaskAsync(long projectId, long taskId, long timeId)
        {
            string url = Urls.HOST + "/" + Urls.DELETE_TIME;
            url = url.Replace("{projectId}", projectId.ToString());
            url = url.Replace("{taskId}", taskId.ToString());
            url = url.Replace("{timeId}", timeId.ToString());
            return await HttpDeleteWithTokenAsync<Void>(url);
        }

        

    }
}
