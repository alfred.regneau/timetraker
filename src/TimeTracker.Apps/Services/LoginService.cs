﻿using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm.Services;
using System;
using System.Threading.Tasks;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos.Authentications;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TimeTracker.Apps.Services
{
    public class LoginService
    {

        public LoginResponse LoginResponse;
        public DateTime ExpirationDate;


        public LoginService()
        {
            string user = Preferences.Get(nameof(LoginService), null);
            if (user != null)
            {
                LoginResponse = JsonConvert.DeserializeObject<LoginResponse>(user);
            }

            string date = Preferences.Get("ExpirationDate", null);
            if (date != null)
            {
                ExpirationDate = JsonConvert.DeserializeObject<DateTime>(date);
            }
            else
            {
                ExpirationDate = DateTime.Now;
            }
        }

        public bool IsLogged()
        {
            return LoginResponse != null && LoginResponse.AccessToken != null && LoginResponse.TokenType != null && LoginResponse.RefreshToken != null;
        }

        public bool IsTokenExpired()
        {
            return ExpirationDate != null && ExpirationDate.Subtract(DateTime.Now).Seconds < 0;
        }

        // return false si le token n'a pas pu être refresh
        public async Task<bool> RefreshTokenAsync()
        {
            if (!IsLogged())
            {
                return false;
            }

            RefreshRequest refreshRequest = new RefreshRequest()
            {
                ClientId = "MOBILE",
                ClientSecret = "COURS",
                RefreshToken =  LoginResponse.RefreshToken
            };
            Response<LoginResponse> response = await DependencyService.Get<RequestService>().RefreshTokenAsync(refreshRequest);

            if (response.IsSuccess)
            {
                LoginResponse = response.Data;
                Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<string> GetAccessToken()
        {
            if (!IsLogged())
            {
                return null;
            }
            else if (IsTokenExpired() && !await RefreshTokenAsync())
            {
                return null;
            }
            return LoginResponse.TokenType + " " + LoginResponse.AccessToken;
        }

        public void Disconnect()
        {
            LoginResponse = new LoginResponse() {
                AccessToken = null,
                ExpiresIn = 0,
                RefreshToken = null,
                TokenType = null
            };
            Save();
        }

        public void Save()
        {
            if (LoginResponse != null)
            {
                string json = JsonConvert.SerializeObject(LoginResponse);
                Preferences.Set(nameof(LoginService), json);
                ExpirationDate = DateTime.Now.AddSeconds(LoginResponse.ExpiresIn);
                string date = JsonConvert.SerializeObject(ExpirationDate);
                Preferences.Set("ExpirationDate", date);
            }
        }
    }
}
