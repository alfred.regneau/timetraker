﻿using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Services;
using Xamarin.Forms;

namespace TimeTracker.Apps.Commands
{
    public class LogoutCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }


        public async void Execute(object parameter)
        {
            if (await new DialogService().DisplayAlertAsync("Déconnexion", "Voulez vous vraiment vous déconnecter ?", "OK", "ANNULER"))
            {
                DependencyService.Get<LoginService>().Disconnect();
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>(mode: NavigationMode.ReplaceAll);
            }
        }
    }
}
