﻿using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.Model
{
    public class Project
    {
        public ICommand ShowTasksCommand { get; }
        public ICommand EditProjectCommand { get; }
        public ICommand DeleteProjectCommand { get; }
        public ProjectItem ProjectItem { get; }
        public Project(ICommand showTasks, ICommand editProject, ICommand deleteProject, ProjectItem projectItem)
        {
            ShowTasksCommand = showTasks;
            EditProjectCommand = editProject;
            DeleteProjectCommand = deleteProject;
            ProjectItem = projectItem;
        }
    }
}