﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.Model
{
    public class TaskModel
    {
        
        public ICommand ShowTaskDetailCommand { get; }
        public ICommand DeleteTaskCommand { get; }
        public ICommand EditTaskCommand { get; }

        public TaskItem TaskItem { get; }
        public DateTime TotalTime { get; }
        public long ProjectId { get; }
        public TaskModel(ICommand showTask, ICommand deleteTask, ICommand editTask, TaskItem taskItem, long projectId)
        {
            TotalTime = new DateTime(0);
            ShowTaskDetailCommand = showTask;
            DeleteTaskCommand = deleteTask;
            EditTaskCommand = editTask;
            TaskItem = taskItem;
            ProjectId = projectId;
            foreach (TimeItem timeItem in TaskItem.Times){
                TotalTime += timeItem.EndTime.Subtract(timeItem.StartTime);
            }
        }
    }
}