﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.Model
{
    public class TimeModel : ContentView
    {
        public ICommand DeleteTimeCommand { get; }
        public ICommand EditTimeCommand { get; }

        public TimeItem TimeItem { get; }
        public long ProjectId { get; }
        public long TaskId { get; }
        public long TimeId { get; }
        public TimeModel(ICommand deleteTime, ICommand editTime, long projectId, long taskId, TimeItem timeItem)
        {
            DeleteTimeCommand = deleteTime;
            EditTimeCommand = editTime;
            ProjectId = projectId;
            TaskId = taskId;
            TimeItem = timeItem;
        }
    }
}