﻿using Storm.Mvvm.Forms;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeTracker.Apps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Apps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListTasksPage : BaseContentPage
    {
        public ListTasksPage()
        {
            InitializeComponent();
            BindingContext = new ListTasksViewModel();
        }
    }
}