﻿using Storm.Mvvm.Forms;
using TimeTracker.Apps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Apps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjectsChartPage : BaseContentPage
    {
        public ProjectsChartPage()
        {
            InitializeComponent();
            BindingContext = new ProjectsChartViewModel();
        }
    }
}