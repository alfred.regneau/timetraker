﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos.Authentications.Credentials;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class EditPasswordViewModel : ViewModelBase
    {

        private string _oldPassword;
        private string _newPassword;

        public string OldPassword { get => _oldPassword; set => SetProperty(ref _oldPassword, value); }
        public string NewPassword { get => _newPassword; set => SetProperty(ref _newPassword, value); }

        public ICommand CancelCommand { get; }
        public ICommand ValidateCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        public EditPasswordViewModel()
        {
            ValidateCommand = new Command(ValidateAction);
            CancelCommand = new Command(CancelAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        private async void ValidateAction()
        {
            if (string.IsNullOrWhiteSpace(OldPassword) || string.IsNullOrWhiteSpace(NewPassword))
            {
                await new DialogService().DisplayAlertAsync("Edition du mot de passe", "Tous les champs doivent être remplis", "OK");
                return;

            }

            SetPasswordRequest setUserProfileRequest = new SetPasswordRequest()
            {
                NewPassword = NewPassword,
                OldPassword = OldPassword
            };

            Response response = await DependencyService.Get<RequestService>().SetPasswordRequestAsync(setUserProfileRequest);
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                await DependencyService.Get<INavigationService>().PopAsync();
            }

        }

        private async void CancelAction()
        {
            await DependencyService.Get<INavigationService>().PopAsync();
        }
    }
}
