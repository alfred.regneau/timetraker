﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Apps.Services;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Authentications.Credentials;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        public ICommand SubmitCommand { get; }
        public ICommand RegisterCommand { get; }

        private string _login;
        private string _password;

        public LoginViewModel()
        {
            SubmitCommand = new Command(LoginAction);
            RegisterCommand = new Command(RegisterAction);
        }

        public string Login { get => _login; set => SetProperty(ref _login, value); }
        public string Password { get => _password; set => SetProperty(ref _password, value); }

        private async void LoginAction()
        {
            if (string.IsNullOrWhiteSpace(Login) || string.IsNullOrWhiteSpace(Password))
            {
                await new DialogService().DisplayAlertAsync("Login", "Tous les champs doivent être remplis", "OK");
                return;
            }

            LoginWithCredentialsRequest loginWithCredentialsRequest = new LoginWithCredentialsRequest()
            {
                ClientId = "MOBILE",
                ClientSecret = "COURS",
                Login = Login,
                Password = Password
            };

            Response<LoginResponse> response = await DependencyService.Get<RequestService>().LoginRequestAsync(loginWithCredentialsRequest);
            if (!response.IsSuccess)
                return;

            LoginService loginService = DependencyService.Get<LoginService>();
            loginService.LoginResponse = response.Data;
            loginService.Save();

            await DependencyService.Get<INavigationService>().PushAsync<MainPage>(mode:NavigationMode.ReplaceAll);
        }

        private void RegisterAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<RegisterPage>();
        }
    }
}
