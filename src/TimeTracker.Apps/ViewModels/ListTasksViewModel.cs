﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Model;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class ListTasksViewModel : ViewModelBase
    {

        private bool _isLoading;

        private long _projectId;
        private string _projectName;
        private ObservableCollection<TaskModel> _tasks;
        private AddTimeRequest _addTimeRequest;
        private bool _addTimeRequestBool;


        public bool IsLoading { get => _isLoading; set => SetProperty(ref _isLoading, value); }

        [NavigationParameter]
        public long ProjectId { get => _projectId; set => SetProperty(ref _projectId, value); }

        [NavigationParameter]
        public string ProjectName { get => _projectName; set => SetProperty(ref _projectName, value); }

        [NavigationParameter]
        public AddTimeRequest AddTimeRequest { get => _addTimeRequest; set => SetProperty(ref _addTimeRequest, value); }


        [NavigationParameter]
        public bool AddTimeRequestBool { get => _addTimeRequestBool; set => SetProperty(ref _addTimeRequestBool, value); }

        public ObservableCollection<TaskModel> Tasks { get => _tasks; set => SetProperty(ref _tasks, value); }

        public ICommand AddTaskCommand { get; }
        public ICommand ShowTasksChartCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }
        public ListTasksViewModel()
        {
            Tasks = new ObservableCollection<TaskModel>();
            AddTaskCommand = new Command(AddTaskAction);
            ShowTasksChartCommand = new Command(ShowTasksChartAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        public override Task OnResume()
        {
            IsLoading = true;
            LoadTasksAsync();
            return base.OnResume();
        }

        public async void LoadTasksAsync()
        {
            Tasks.Clear();
            RequestService requestService = DependencyService.Get<RequestService>();
            Response<ObservableCollection<TaskItem>> response = await requestService.ListTasksRequestAsync(ProjectId);

            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                foreach (TaskItem taksItem in response.Data)
                {
                    Tasks.Add(new TaskModel(new Command<TaskModel>(ShowTaskDetail),
                                            new Command<TaskModel>(DeleteTaskAction),
                                            new Command<TaskModel>(EditTaskAction),
                                            taksItem, ProjectId));
                }
                IsLoading = false;
            }
        }

        private void AddTaskAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<AddTaskPage>(new Dictionary<string, object>()
            {
                ["ProjectId"] = ProjectId,
                ["ProjectName"] = ProjectName
            });
        }

        private void EditTaskAction(TaskModel task)
        {
            DependencyService.Get<INavigationService>().PushAsync<EditTaskPage>(new Dictionary<string, object>()
            {
                ["TaskModel"] = task
            }) ;
        }

        public async void DeleteTaskAction(TaskModel task)
        {
            if (await new DialogService().DisplayAlertAsync(task.TaskItem.Name, "Voulez vous vraiment supprimer cette tâche ?", "OK", "ANNULER"))
            {
                Response response = await DependencyService.Get<RequestService>().DeleteTaskAsync(ProjectId, task.TaskItem.Id);
                if (response == null)
                {
                    await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                    return;
                }
                else if (response.IsSuccess)
                {
                    LoadTasksAsync();
                }
            }
            
        }

        public void ShowTaskDetail(TaskModel task)
        {
            if (AddTimeRequestBool)
            {
                DependencyService.Get<INavigationService>().PushAsync<TaskDetailPage>(new Dictionary<string, object>()
                {
                    ["ProjectId"] = task.ProjectId,

                    ["TaskId"] = task.TaskItem.Id,
                    ["TaskName"] = task.TaskItem.Name,
                    ["TotalTime"] = task.TotalTime,
                    ["AddTimeRequestBool"] = true,
                    ["AddTimeRequest"] = AddTimeRequest

                });
            }
            else
            {
                DependencyService.Get<INavigationService>().PushAsync<TaskDetailPage>(new Dictionary<string, object>()
                {
                    ["ProjectId"] = task.ProjectId,

                    ["TaskId"] = task.TaskItem.Id,
                    ["TaskName"] = task.TaskItem.Name,
                    ["TotalTime"] = task.TotalTime,
                    ["AddTimeRequestBool"] = false

                });
            }
        }

        private void ShowTasksChartAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<TasksChartPage>(new Dictionary<string, object>()
            {
                ["Tasks"] = Tasks
            });
        }
    }
}