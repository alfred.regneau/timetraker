﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Model;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class TaskDetailViewModel : ViewModelBase
    {
        private DateTime _totalTimes;
        private DateTime _currentTimer;
        private long _taskId;
        private long _projectId;
        private string _taskName;
        private string _timerString;
        private string _timerButton;
        private bool _addTimeRequestBool;
        private AddTimeRequest _addTimeRequest;

        [NavigationParameter]
        public DateTime TotalTime { get => _totalTimes; set => SetProperty(ref _totalTimes, value); }
        public DateTime CurrentTimer { get => _currentTimer; set => SetProperty(ref _currentTimer, value); }

        [NavigationParameter]
        public long TaskId { get => _taskId; set => SetProperty(ref _taskId, value); }

        [NavigationParameter]
        public long ProjectId { get => _projectId; set => SetProperty(ref _projectId, value); }

        [NavigationParameter]
        public AddTimeRequest AddTimeRequest { get => _addTimeRequest; set => SetProperty(ref _addTimeRequest, value); }

        [NavigationParameter]
        public bool AddTimeRequestBool { get => _addTimeRequestBool; set => SetProperty(ref _addTimeRequestBool, value); }

        [NavigationParameter]
        public string TaskName { get => _taskName; set => SetProperty(ref _taskName, value); }

        public string TimerString { get => _timerString; set => SetProperty(ref _timerString, value); }
        public string TimerButton { get => _timerButton; set => SetProperty(ref _timerButton, value); }
        public string TextAddTime { get => _textAddTime; set => SetProperty(ref _textAddTime, value); }


        public bool IsRunning { get; set; }
        public Timer Timer { get; set; }

        public ICommand TimerCommand { get; }
        public ICommand AddTimeCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        private DateTime StartTime;
        private DateTime EndTime;
        private String _textAddTime;

        public TaskDetailViewModel()
        {
            TimerCommand = new Command(StartOrPauseTimer);
            TimerString = "";
            TimerButton = "play";
            IsRunning = false;
            AddTimeCommand = new Command(AddTimeAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();

        }

        public override Task OnResume()
        {
            if (AddTimeRequestBool)
            {
                TimeSpan timeSpan = AddTimeRequest.EndTime.Subtract(AddTimeRequest.StartTime);
                TextAddTime =  string.Format("Ajouter {0:HH:mm:ss} à cette tâche", new DateTime(timeSpan.Ticks));
            }
            return base.OnResume();
        }

        private async void AddTimeAction()
        {
            Response<TimeItem> response = await DependencyService.Get<RequestService>().AddTimeTaskAsync(AddTimeRequest, ProjectId, TaskId);
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                TimeItem timeItem = response.Data;
                TotalTime += timeItem.EndTime.Subtract(timeItem.StartTime);
            }
            AddTimeRequest = null;
            AddTimeRequestBool = false;
        }
        private async void SaveTaskTimeAsync()
        {
            AddTimeRequest addTimeRequest = new AddTimeRequest()
            {
                StartTime = StartTime,
                EndTime = EndTime
            };
            Response<TimeItem> response = await DependencyService.Get<RequestService>().AddTimeTaskAsync(addTimeRequest, ProjectId, TaskId);
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                TimeItem timeItem = response.Data;
                TotalTime += timeItem.EndTime.Subtract(timeItem.StartTime);
            }
        }

        private void StartOrPauseTimer()
        {
            if (!IsRunning)
            {
                StartTime = DateTime.Now;
                TimerButton = "pause";
                IsRunning = true;
                Timer = new Timer();
                Timer.Interval = 10; // 10 milliseconds
                Timer.Elapsed += TimerElapsed;
                Timer.Start();
            }
            else
            {
                EndTime = DateTime.Now;
                Timer.Stop();
                TimerButton = "play";
                IsRunning = false;
                Timer = null;
                SaveTaskTimeAsync();
            }
        }


        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            TimeSpan timeSpan = DateTime.Now.Subtract(StartTime);
            CurrentTimer = new DateTime(timeSpan.Ticks);
        }
    }
}