﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Model;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class EditTaskViewModel : ViewModelBase
    {
        private TaskModel _taskModel;

        [NavigationParameter]
        public TaskModel TaskModel { get => _taskModel; set => SetProperty(ref _taskModel, value); }

        private ObservableCollection<TimeModel> _listTime;
        public ObservableCollection<TimeModel> ListTime { get => _listTime; set => SetProperty(ref _listTime, value); }

        public ICommand EditCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        public EditTaskViewModel()
        {
            ListTime = new ObservableCollection<TimeModel>();
            EditCommand = new Command(EditAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        public override Task OnResume()
        {
            ListTime.Clear();
            LoadTaskTimes();
            return base.OnResume();
        }

        public void LoadTaskTimes()
        {
            ListTime.Clear();
            foreach (TimeItem timeItem in TaskModel.TaskItem.Times)
            {
                ListTime.Add(new TimeModel(new Command<TimeModel>(DeleteTimesTaskAction), new Command<TimeModel>(EditTimesAction), TaskModel.ProjectId, TaskModel.TaskItem.Id, timeItem));
            }
        }

        public async void DeleteTimesTaskAction(TimeModel timeModel)
        {

            if (await new DialogService().DisplayAlertAsync("", "Voulez vous vraiment supprimer ce temps ?", "OK", "ANNULER"))
            {

                Response response = await DependencyService.Get<RequestService>().DeleteTimeTaskAsync(timeModel.ProjectId, timeModel.TaskId, timeModel.TimeItem.Id);
                if (response == null)
                {
                    await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                    return;
                }
                else if (response.IsSuccess)
                {
                    TaskModel.TaskItem.Times.Remove(timeModel.TimeItem);
                    LoadTaskTimes();
                }
            }
        }

        private void EditTimesAction(TimeModel timeModel)
        {
            DependencyService.Get<INavigationService>().PushAsync<EditTimes>(new Dictionary<string, object>()
            {
                ["TimeModel"] = timeModel,
                ["Hours"] = timeModel.TimeItem.EndTime.Hour,
                ["Minutes"] = timeModel.TimeItem.EndTime.Minute,
                ["Seconds"] = timeModel.TimeItem.EndTime.Second
            });
        }

        private async void EditAction()
        {
            if (string.IsNullOrWhiteSpace(TaskModel.TaskItem.Name))
            {
                await new DialogService().DisplayAlertAsync("Edition d'une tâche", "Tous les champs doivent être remplis", "OK");
                return;
            }

            AddTaskRequest addTaskRequest = new AddTaskRequest()
            {
                Name = TaskModel.TaskItem.Name
            };

            Response<TaskItem> response = await DependencyService.Get<RequestService>().PutTaskRequestAsync(addTaskRequest, TaskModel.ProjectId, TaskModel.TaskItem.Id);
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                await DependencyService.Get<INavigationService>().PopAsync();
            }
        }
    }
}