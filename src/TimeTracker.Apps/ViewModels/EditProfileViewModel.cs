﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Apps.Services;
using TimeTracker.Dtos.Accounts;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class EditProfileViewModel : ViewModelBase
    {
        private string _email;
        private string _firstName;
        private string _lastName;
        private bool _isLoading;

        public string Email { get => _email; set => SetProperty(ref _email, value); }
        public string FirstName { get => _firstName; set => SetProperty(ref _firstName, value); }
        public string LastName { get => _lastName; set => SetProperty(ref _lastName, value); }
        public bool IsLoading { get => _isLoading; set => SetProperty(ref _isLoading, value); }

        public ICommand CancelCommand { get; }
        public ICommand ValidateCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        public EditProfileViewModel()
        {
            ValidateCommand = new Command(ValidateAction);
            CancelCommand = new Command(CancelAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
            Email = "";
            FirstName = "";
            LastName = "";
        }

        public override Task OnResume()
        {
            IsLoading = true;
            LoadUserAsync();
            return base.OnResume();
        }

        private async void LoadUserAsync()
        {
            RequestService requestService = DependencyService.Get<RequestService>();
            Response<UserProfileResponse> response = await requestService.UserProfileRequestAsync();
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            if (response.IsSuccess)
            {
                Email = response.Data.Email;
                FirstName = response.Data.FirstName;
                LastName = response.Data.LastName;
                IsLoading = false;
            }
        }

        private async void ValidateAction()
        {
            if (string.IsNullOrWhiteSpace(Email) || string.IsNullOrWhiteSpace(FirstName) || string.IsNullOrWhiteSpace(LastName))
            {
                await new DialogService().DisplayAlertAsync("Edition du profil", "Tous les champs doivent être remplis", "OK");
                return;
                
            }

            SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest()
            {
                Email = Email,
                FirstName = FirstName,
                LastName = LastName
            };

            Response<UserProfileResponse> response = await DependencyService.Get<RequestService>().SetUserProfileRequestAsync(setUserProfileRequest);
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                await DependencyService.Get<INavigationService>().PopAsync();
            }

        }

        private async void CancelAction()
        {
            await DependencyService.Get<INavigationService>().PopAsync();
        }
    }
}
