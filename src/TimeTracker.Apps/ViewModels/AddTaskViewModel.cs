﻿using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class AddTaskViewModel : ViewModelBase
    {
        public ICommand ValidateCommand { get; }
        public ICommand CancelCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        private string _name;
        private long _projectId;
        private string _projectName;


        public string Name { get => _name; set => SetProperty(ref _name, value); }

        [NavigationParameter]
        public long ProjectId { get => _projectId; set => SetProperty(ref _projectId, value); }

        [NavigationParameter]
        public string ProjectName { get => _projectName; set => SetProperty(ref _projectName, value); }

        public AddTaskViewModel()
        {
            ValidateCommand = new Command(ValidateAction);
            CancelCommand = new Command(CancelAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        private void CancelAction()
        {
            DependencyService.Get<INavigationService>().PopAsync();
        }

        private async void ValidateAction()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                await new DialogService().DisplayAlertAsync("Tâche", "Tous les champs doivent être remplis", "OK");
                return;
            }

            AddTaskRequest addTaskRequest = new AddTaskRequest()
            {
                Name = Name
            };
            Response<TaskItem> response = await DependencyService.Get<RequestService>().AddTaskRequestAsync(addTaskRequest, ProjectId);
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if (response.IsSuccess)
            {
                await DependencyService.Get<INavigationService>().PopAsync();
            }
        }
    }
}