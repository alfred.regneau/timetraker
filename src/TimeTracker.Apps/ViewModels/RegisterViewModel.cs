﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Apps.Services;
using TimeTracker.Dtos.Accounts;
using TimeTracker.Dtos.Authentications;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class RegisterViewModel : ViewModelBase
    {
        public ICommand SubmitCommand { get; }
        public ICommand LoginCommand { get; }

        private string _email;
        private string _firstName;
        private string _lastName;
        private string _password;

        public RegisterViewModel()
        {
            SubmitCommand = new Command(RegisterAction);
            LoginCommand = new Command(LoginAction);
        }

        public string Email { get => _email; set => SetProperty(ref _email, value); }
        public string FirstName { get => _firstName; set => SetProperty(ref _firstName, value); }
        public string LastName { get => _lastName; set => SetProperty(ref _lastName, value); }
        public string Password { get => _password; set => SetProperty(ref _password, value); }

        private async void RegisterAction()
        {
            if (string.IsNullOrWhiteSpace(Email) || string.IsNullOrWhiteSpace(FirstName) || string.IsNullOrWhiteSpace(LastName) || string.IsNullOrWhiteSpace(Password))
            {
                await new DialogService().DisplayAlertAsync("Inscription", "Tous les champs doivent être remplis", "OK");
                return;
            }

            CreateUserRequest createUserRequest =  new CreateUserRequest()
            {
                ClientId = "MOBILE",
                ClientSecret = "COURS",
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                Password = Password
            };

            Response<LoginResponse> response = await DependencyService.Get<RequestService>().RegisterRequestAsync(createUserRequest);

            if (!response.IsSuccess)
                return;

            LoginService loginService = DependencyService.Get<LoginService>();
            loginService.LoginResponse = response.Data;
            loginService.Save();
            await DependencyService.Get<INavigationService>().PushAsync<MainPage>(mode:NavigationMode.ReplaceAll);
        }

        private void LoginAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
        }
    }
}
