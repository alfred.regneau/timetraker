﻿using Microcharts;
using Microcharts.Forms;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Model;

namespace TimeTracker.Apps.ViewModels
{
    public class TasksChartViewModel : ViewModelBase
    {

        private ObservableCollection<TaskModel> _tasks;
        private ObservableCollection<ChartEntry> _entries;

        private Chart _donutChartTimes;

        private Chart _barChartTimes;

        private int _barChartWidth;

        [NavigationParameter]
        public ObservableCollection<TaskModel> Tasks { get => _tasks; set => SetProperty(ref _tasks, value); }
        public ObservableCollection<ChartEntry> Entries { get => _entries; set => SetProperty(ref _entries, value); }
        public Chart DonutChartTimes { get => _donutChartTimes; set => SetProperty(ref _donutChartTimes, value); }
        public Chart BarChartTimes { get => _barChartTimes; set => SetProperty(ref _barChartTimes, value); }
        public int BarChartWidth { get => _barChartWidth; set => SetProperty(ref _barChartWidth, value); }

        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }
        public TasksChartViewModel()
        {
            Entries = new ObservableCollection<ChartEntry>();
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }
        public override Task OnResume()
        {
            Entries.Clear();
            Random rnd = new Random();
            foreach (TaskModel task in Tasks)
            {
                Entries.Add(new ChartEntry(task.TotalTime.Ticks)
                {
                    Label = string.Format("ID{0}", task.TaskItem.Id),
                    ValueLabel = string.Format("{0:HH:mm}", task.TotalTime),
                    Color = SkiaSharp.SKColor.Parse(string.Format("#{0:X6}", rnd.Next(0x1000000)))
                });
            }
            if (Entries.Count > 0)
            {
                BarChartTimes = new BarChart
                {
                    LabelTextSize = 40f,
                    ValueLabelTextSize = 40f,
                    Entries = Entries
                };
                BarChartWidth = Entries.Count * 25;
                DonutChartTimes = new DonutChart
                {
                    Entries = Entries,
                    LabelMode = LabelMode.None
                };
            }
            return base.OnResume();
        }
    }
}
