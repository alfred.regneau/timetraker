using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Apps.Services;
using TimeTracker.Dtos.Accounts;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Essentials;
using System.Timers;
using Xamarin.Forms;
using System.Collections.Generic;
using TimeTracker.Apps.Commands;

namespace TimeTracker.Apps.ViewModels
{
    public class MainViewModel : ViewModelBase
    {

        private bool _isLoading;
        private string _userName;
        private string _timerString;
        private string _timerButton;
        private DateTime _currentTimer;

        public bool IsLoading { get => _isLoading; set => SetProperty(ref _isLoading, value); }
        public DateTime CurrentTimer { get => _currentTimer; set => SetProperty(ref _currentTimer, value); }
        public string UserName { get => _userName; set => SetProperty(ref _userName, value); }

        public string TimerString { get => _timerString; set => SetProperty(ref _timerString, value); }
        public string TimerButton { get => _timerButton; set => SetProperty(ref _timerButton, value); }

        public ICommand EditProfileCommand { get; }
        public ICommand EditPasswordCommand { get; }
        public ICommand ListProjectsCommand { get; }
        public ICommand DisconnectCommand { get; }
        public ICommand LogoutCommand { get; }
        public bool IsRunning { get; set; }
        public Timer Timer { get; set; }
        public ICommand TimerCommand { get; }

        private DateTime StartTime;
        private DateTime EndTime;

        public MainViewModel()
        {
            EditProfileCommand = new Command(EditProfileAction);
            EditPasswordCommand = new Command(EditPasswordAction);
            ListProjectsCommand = new Command(ListProjectAction);
            DisconnectCommand = new Command(DisconnectAction);
            TimerCommand = new Command(StartOrPauseTimer);
            LogoutCommand = new LogoutCommand();
            TimerString = "";
            TimerButton = "play";
            IsRunning = false;
        }

        public override Task OnResume()
        {
            IsLoading = true;
            LoadUserAsync();
            return base.OnResume();
        }

        private async void LoadUserAsync()
        {
            RequestService requestService = DependencyService.Get<RequestService>();
            Response<UserProfileResponse> response = await requestService.UserProfileRequestAsync();
            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            if (response.IsSuccess)
            {
                UserName = response.Data.FirstName;
                IsLoading = false;
            }
        }

        private void EditProfileAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<EditProfilePage>();
        }

        private void EditPasswordAction(object obj)
        {
            DependencyService.Get<INavigationService>().PushAsync<EditPasswordPage>();
        }

        private void ListProjectAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<ListProjectsPage>(new Dictionary<string, object>()
            {
                ["AddTimeRequestBool"] = false
            });
        }
        private void DisconnectAction()
        {
            LoginService loginService = DependencyService.Get<LoginService>();
            loginService.Disconnect();
            DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
        }

        private async void SaveTaskTimeAsync()
        {
            AddTimeRequest addTimeRequest = new AddTimeRequest()
            {
                StartTime = StartTime,
                EndTime = EndTime
            };
            TimeSpan timeSpan = EndTime.Subtract(StartTime);
            await new DialogService().DisplayAlertAsync(
                string.Format("{0:HH:mm:ss:ff}", 
                new DateTime(timeSpan.Ticks)), 
                "Vous pouvez ajouter ce temps � une t�che en cliquant",
                "OK");
            await DependencyService.Get<INavigationService>().PushAsync<ListProjectsPage>(new Dictionary<string, object>() { 
                ["AddTimeRequest"] = addTimeRequest,
                ["AddTimeRequestBool"] = true
            });


        }

        private void StartOrPauseTimer()
        {
            if (!IsRunning)
            {
                StartTime = DateTime.Now;
                TimerButton = "pause";
                IsRunning = true;
                Timer = new Timer();
                Timer.Interval = 10; // 10 milliseconds
                Timer.Elapsed += TimerElapsed;
                Timer.Start();
            }
            else
            {
                EndTime = DateTime.Now;
                Timer.Stop();
                TimerButton = "play";
                IsRunning = false;
                Timer = null;
                SaveTaskTimeAsync();
                StartTime = DateTime.Now;
            }
        }


        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            TimeSpan timeSpan = DateTime.Now.Subtract(StartTime);
            CurrentTimer = new DateTime(timeSpan.Ticks);
        }
    }
    
}
