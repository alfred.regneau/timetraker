﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Model;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels 
{
    public class ListProjectsViewModel : ViewModelBase
    {
        private bool _isLoading;
        private AddTimeRequest _addTimeRequest;
        private bool _addTimeRequestBool;
        private ObservableCollection<Project> _projects;

        public ObservableCollection<Project> Projects { get => _projects; set => SetProperty(ref _projects, value); }
        public bool IsLoading { get => _isLoading; set => SetProperty(ref _isLoading, value); }

        [NavigationParameter]
        public AddTimeRequest AddTimeRequest { get => _addTimeRequest; set => SetProperty(ref _addTimeRequest, value); }

        [NavigationParameter]
        public bool AddTimeRequestBool { get => _addTimeRequestBool; set => SetProperty(ref _addTimeRequestBool, value); }

        public ICommand AddCommand { get; }
        public ICommand ShowProjectsChartCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        public ListProjectsViewModel()
        {
            Projects = new ObservableCollection<Project>();

            AddCommand = new Command(AddProjectAction);
            ShowProjectsChartCommand = new Command(ShowProjectsChartAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        public override Task OnResume()
        {
            IsLoading = true;
            LoadProjectsAsync();
            return base.OnResume();
        }

        public async void LoadProjectsAsync()
        {
            Projects.Clear();
            RequestService requestService = DependencyService.Get<RequestService>();
            Response<ObservableCollection<ProjectItem>> response = await requestService.ListProjectsRequestAsync();

            if (response == null)
            {
                await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                return;
            }
            else if(response.IsSuccess)
            {
                ObservableCollection<ProjectItem> projectItems = response.Data;
                Projects.Clear();
                foreach (ProjectItem projectItem in projectItems)
                {
                    Projects.Add(new Project(new Command<Project>(ShowTasksAction), new Command<Project>(EditProjectAction), new Command<Project>(DeleteProjectAction), projectItem));
                }
                IsLoading = false;
            }

        }

        private void AddProjectAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<AddProjectPage>(new Dictionary<string, object>()
            {
                ["ProjectId"] = -1,
                ["ProjectName"] = "",
                ["ProjectDesc"] = ""
            });
        }

        private void EditProjectAction(Project project)
        {
            DependencyService.Get<INavigationService>().PushAsync<AddProjectPage>(new Dictionary<string, object>()
            {
                ["ProjectId"] = project.ProjectItem.Id,
                ["ProjectName"] = project.ProjectItem.Name,
                ["ProjectDesc"] = project.ProjectItem.Description
            });
        }

        private async void DeleteProjectAction(Project project)
        {
            if (await new DialogService().DisplayAlertAsync(project.ProjectItem.Name, "Voulez vous vraiment supprimer ce projet ?", "OK", "ANNULER"))
            {
                Response response = await DependencyService.Get<RequestService>().DeleteProjectAsync(project.ProjectItem.Id);
                if (response == null)
                {
                    await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                    return;
                }
                else if (response.IsSuccess)
                {
                    LoadProjectsAsync();
                }
            }
        }


        public void ShowTasksAction(Project project)
        {
            if (!AddTimeRequestBool)
            {
                DependencyService.Get<INavigationService>().PushAsync<ListTasksPage>(new Dictionary<string, object>()
                {
                    ["ProjectId"] = project.ProjectItem.Id,
                    ["ProjectName"] = project.ProjectItem.Name,
                    ["AddTimeRequestBool"] = false
                });
            }
            else
            {
                DependencyService.Get<INavigationService>().PushAsync<ListTasksPage>(new Dictionary<string, object>()
                {
                    ["ProjectId"] = project.ProjectItem.Id,
                    ["ProjectName"] = project.ProjectItem.Name,
                    ["AddTimeRequest"] = AddTimeRequest,
                    ["AddTimeRequestBool"] = true
                });
            }
            
        }


        private void ShowProjectsChartAction()
        {
            DependencyService.Get<INavigationService>().PushAsync<ProjectsChartPage>(new Dictionary<string, object>()
            {
                ["Projects"] = Projects
            });
        }
    }
}
