﻿using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Apps.Services;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class AddProjectViewModel : ViewModelBase
    {
        private string _name;
        private string _description;
        private long _projectId;
        private string _projectName;
        private string _projectDesc;
        public ICommand ValidateCommand { get; }
        public ICommand CancelCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        public string Name { get => _name; set => SetProperty(ref _name, value); }
        public string Description { get => _description; set => SetProperty(ref _description, value); }

        [NavigationParameter]
        public long ProjectId { get => _projectId; set => SetProperty(ref _projectId, value); }

        [NavigationParameter]
        public string ProjectName { get => _projectName; set => SetProperty(ref _projectName, value); }

        [NavigationParameter]
        public string ProjectDesc { get => _projectDesc; set => SetProperty(ref _projectDesc, value); }

        public AddProjectViewModel()
        {
            ValidateCommand = new Command(ValidateAction);
            CancelCommand = new Command(CancelAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        public override Task OnResume()
        {
            if (ProjectId >= 0)
            {
                Name = ProjectName;
                Description = ProjectDesc;
            }
            return base.OnResume();
        }

        private async void ValidateAction()
        {
            if (string.IsNullOrWhiteSpace(Name) || string.IsNullOrWhiteSpace(Description))
            {
                await new DialogService().DisplayAlertAsync("Projet", "Tous les champs doivent être remplis", "OK");
                return;
            }

            AddProjectRequest addProjectRequest = new AddProjectRequest()
            {
                Name = Name,
                Description = Description
            };

            if (ProjectId <= 0)
            {
                

                Response<ProjectItem> response = await DependencyService.Get<RequestService>().AddProjectAsync(addProjectRequest);
                if (response == null)
                {
                    await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                    return;
                }
                else if (response.IsSuccess)
                {
                    await DependencyService.Get<INavigationService>().PopAsync();
                }
            }
            else
            {
                Response<ProjectItem> response = await DependencyService.Get<RequestService>().PutProjectAsync(addProjectRequest, ProjectId);
                if (response == null)
                {
                    await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                    return;
                }
                else if (response.IsSuccess)
                {
                    await DependencyService.Get<INavigationService>().PopAsync();
                }
            }
            

        }

        private void CancelAction()
        {
            DependencyService.Get<INavigationService>().PopAsync();
        }
    }
}