﻿using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Apps.Commands;
using TimeTracker.Apps.Model;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class EditTimesViewModel : ViewModelBase
    {
        private TimeModel _timeModel;
        private int _hours;
        private int _minutes;
        private int _seconds;
        private TimeSpan _timeSpanEnd;
        public ICommand ValiderCommand { get; }
        public ICommand HomeCommand { get; }
        public ICommand LogoutCommand { get; }

        [NavigationParameter]
        public TimeModel TimeModel { get => _timeModel; set => SetProperty(ref _timeModel, value); }

        [NavigationParameter]
        public int Hours { get => _hours; set => SetProperty(ref _hours, value); }

        [NavigationParameter]
        public int Minutes { get => _minutes; set => SetProperty(ref _minutes, value); }

        [NavigationParameter]
        public int Seconds { get => _seconds; set => SetProperty(ref _seconds, value); }

        public TimeSpan TimeSpanEnd { get => _timeSpanEnd; set => SetProperty(ref _timeSpanEnd, value); }

        public EditTimesViewModel()
        {
            ValiderCommand = new Command(ValiderAction);
            HomeCommand = new HomeCommand();
            LogoutCommand = new LogoutCommand();
        }

        public override Task OnResume()
        {
            if(TimeModel.TimeId >= 0)
            {
                TimeSpanEnd = new TimeSpan(Hours, Minutes, Seconds);
            }
            return base.OnResume();
        }

        private async void EditTimeAction()
        {
            DateTime newEnd = new DateTime(TimeModel.TimeItem.EndTime.Year, TimeModel.TimeItem.EndTime.Month, TimeModel.TimeItem.EndTime.Day, TimeSpanEnd.Hours, TimeSpanEnd.Minutes, TimeSpanEnd.Seconds);

            if (DateTime.Compare(TimeModel.TimeItem.StartTime, newEnd) < 0)
            {
                AddTimeRequest addTimeRequest = new AddTimeRequest()
                {
                    StartTime = TimeModel.TimeItem.StartTime,
                    EndTime = newEnd
                };
                Response<TimeItem> response = await DependencyService.Get<RequestService>().PutTimeTaskAsync(addTimeRequest, TimeModel.ProjectId, TimeModel.TaskId, TimeModel.TimeItem.Id);
                if (response == null)
                {
                    await DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
                    return;
                }
                else if (response.IsSuccess)
                {
                    TimeModel.TimeItem.EndTime = newEnd;
                    await DependencyService.Get<INavigationService>().PopAsync();
                }
            }
            else
            {
                await new DialogService().DisplayAlertAsync("Projet", "La date de fin ne peut pas être inférieure à la date du début ( à savoir "+ TimeModel.TimeItem.StartTime +" )", "OK");
            }
            
        }

        private void ValiderAction()
        {
            EditTimeAction();
        }
    }
}