﻿using Storm.Mvvm;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Requests;
using TimeTracker.Apps.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace TimeTracker.Apps
{
    public partial class App : MvvmApplication
    {
        public App() : base(CreateStartPage)
        {
            InitializeComponent();
        }

        private static Page CreateStartPage()
        {
            DependencyService.Register<LoginService>();
            DependencyService.Register<RequestService>();

            LoginService loginService = DependencyService.Get<LoginService>();
            if (loginService.IsLogged())
            {
                return new MainPage();
            }
            else
            {
                return new LoginPage();
            }

        }
    }
}