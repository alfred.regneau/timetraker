using System.Collections.Generic;
using Newtonsoft.Json;
using Storm.Mvvm;

namespace TimeTracker.Dtos.Projects
{
    public class TaskItem : NotifierBase
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("times")]
        public List<TimeItem> Times { get; set; }
    }
}